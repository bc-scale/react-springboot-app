# React-Springboot-App
# Task 2


Тренировочный проект для сборки frontend и backend компонентов приложения в Docker

## Пример собранного приложения
1. Проект: [http://oleg-do.devops.rebrain.srwx.net](http://oleg-do.devops.rebrain.srwx.net)
2. Adminer (для работы с БД): [http://oleg-do.devops.rebrain.srwx.net/adminer](http://oleg-do.devops.rebrain.srwx.net/adminer)

## Технологии

- ReactJs
- Spring boot

## Docker
Для докеризации приложения создается 2 основных микросервиса (`front` и `back`) и 3 вспомогательных (`db`, `nginx-proxy` и `adminer`).
Ручной запуск сборки:
```sh
  docker compose up -d
```
## GitLab CI/CD
1. Реализована сборка образов `front` и `back` через GitLab CI/CD.
2. Реализован деплой на тестовый сервер [http://oleg-do.devops.rebrain.srwx.net](http://oleg-do.devops.rebrain.srwx.net) по коммиту.
- Для удобства стадия сборки переведена в режим `manual.
- Проверяются изменения файлов исходников и Dockerfile для сборки `front` и `back`.

### Особенности
1. Балансировщик закрепляет сессию через директиву upstream `ip_hash`.
2. Реализовано автоматическое добавление нод в пулы апстримов через `nginx-proxy`. В образ добавлен скрипт для добавления `ip_hash` в `nginx.tmpl`.
3. Добавлены HEALTHCHECK во все контейнеры.
